package ru.b3nder;

import java.util.Set;

public interface IFrequencyCallObject<KeyType> {
    Set<KeyType> getMostFrequentlyUsedKeys();
    int getFrequencyOfCallingObject(KeyType key);
}
